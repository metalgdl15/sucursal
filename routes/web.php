<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('producto', 'ProductoController');
Route::resource('usuario', 'UserController');
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/busqueda','ProductoController@buscar')->name('producto.search');
Route::post('/compra/{id}','VentaController@addProducto')->name('venta.producto');
Route::get('/detalle/{id}','VentaController@viewProducto')->name('venta.detalle');
Route::resource('venta', 'VentaController');
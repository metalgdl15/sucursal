<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Image;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index()
    {
        $producto= Producto::paginate(10);

        return view ('productos.indexProductos', compact('producto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view("productos.formProductos");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto(); 

        //Proceso para las imagenes de los productos

        $this->validate($request,['photo'=>'required|image']);

        $imagen = $request->file('photo');
        $extension= $imagen->getClientOriginalExtension();
        $file_name= $this->random_string() . '.' . $extension;

        Image::make($imagen)
            ->resize(144,144)
            ->save('img/productos/'.$file_name);

        $producto->photo = $file_name;

        $request->validate([
          'nombre' => 'required|min:5',
          'descripcion' => 'required|min:10',
          'existencia' => 'required|integer',
          'cantidad' => 'required|integer',
          'precio' => 'required|integer'
        ]);

        $producto->Nombre = $request->nombre;
        $producto->Descripcion = $request->descripcion;
        $producto->Cantidad = $request->cantidad;
        $producto->Existencia = $request->existencia;
        $producto->Precio = $request->precio;

        $producto->save();

        return redirect()->route('producto.index')->with(['mensaje' => 'Producto Registrado', 'alert-class' => 'alert-success']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        //$producto = Producto::All();
        return view ('productos.showProductos', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        return view('productos.formProductos')->with(['producto' => $producto]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {

        Producto::where('id', $producto->id)->update($request->except('_token', '_method'));
      
        return redirect()->route('producto.show', $producto->id);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return redirect()->route('producto.index');
    }


    
/*    public function getPhoto($request){
        if ($request->photo)
            return 'img/productos/'.$request->id.'.'.$request->photo;

        return 'img/productos/signo.jpg';
    }
    */

    protected function random_string(){
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );
 
        for($i=0; $i<10; $i++){
            $key .= $keys[array_rand($keys)];
        }
 
            return $key;
    }

    public function buscar(Request $request){
        $datos = $request->busqueda;
        $producto = Producto::where('nombre', 'like','%'.$datos.'%')->paginate(10);

        return view ('productos.searchProductos', compact('producto'));
    }
}

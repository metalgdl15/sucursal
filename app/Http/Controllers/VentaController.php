<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use App\Venta;
use App\Producto;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$ventas= Venta::all();
        //$user = User::find(\Auth::id());

        $ventas = Venta::where('idUsuario', \Auth::id())->with('productos')->get();
        
        return view('ventas.indexVentas', compact('ventas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ventas.formVentas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now =  Carbon::now();
       
        $ventas = new Venta();
        $ventas->fecha = $now->format('Y-m-d');
        $ventas->total = $request->total;
        $ventas->idUsuario= $request->usuario;
        $ventas->save();

        return redirect()->route('venta.index');
            

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show(Venta $venta)
    {

       return view('ventas.showVentas', compact('venta'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(Venta $venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Venta $venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venta $venta)
    {
        //
    }

    public function addProducto(Request $request, $id){
        
        $producto = Producto::find($id);
        $user = User::find(\Auth::id());

         $now =  Carbon::now();
       
        $ventas = new Venta();        

        $ventas->fecha = $now->format('Y-m-d');
        $ventas->total = $producto->precio;
        $ventas->idUsuario= $user->id;
        
        $ventas->save();



        $ventas->Productos()->attach($producto);

        return redirect()->route('venta.index');
    }

    public function viewProducto(Request $request, $id){
        $producto = Producto::find($id);

        return view ('productos.showProductos', compact('producto'));
    }
}

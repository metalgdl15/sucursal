<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
	protected $primaryKey = 'idVenta';

    public function productos(){
    	return $this->belongsToMany(Producto::class, 'producto_venta', 'venta_id', 'producto_id');
    }
    public function Users(){
    	return $this->belongsToMany(User::Class);
    }

}

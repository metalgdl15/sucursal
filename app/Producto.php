<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable =['nombre','descripcion', 'cantidad', 'existencia', 'precio', 'photo'];

    public function Ventas(){
    	return $this->belongsToMany(Venta::class);
    }

    public function Compras(){
    	return $this->belongsToMany(Compra::Class);
    }
}

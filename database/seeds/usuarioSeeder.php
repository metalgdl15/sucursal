<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class usuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'alexis',
            'email'=>'francisco_cleal@alumnos.udg.mx',
            'password'=>bcrypt('guadalajara'),
        ]);
    }
}

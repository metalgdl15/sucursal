<?php

use Faker\Generator as Faker;
use App\Producto;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->text,
        'cantidad' => rand(1, 30),
        'existencia' => rand(1, 0),
        'precio' => rand(100, 1000),
        'photo' => "signo.jpg",

    ];
});

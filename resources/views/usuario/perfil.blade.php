

<?php
/**
 * Created by PhpStorm.
 * User: AlexisCruz
 * Date: 01/12/2018
 * Time: 18:23
 */
?>

@extends ('layouts.tema')
@section('contenido')
<div >
<h2> Hola mundo {{ Auth::user()->name }}</h2>
</div>
<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
            <tr>
                <th>  </th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
            </tr>
            </thead>

            <tbody>
            @foreach($producto as $prod)
                <tr>
                    <td>
                        <a class="btn btn-sm bt-info btn-primary" href ="{{route('producto.show', $prod->id)}}"> Ver Detalles </a>
                    </td>

                    <td>{{ $prod->nombre }}</td>
                    <td>{{ $prod->descripcion }} </td>
                    <td>{{ $prod->precio }} </td>
                    <td>
                        <img src="/img/productos/{{$prod->photo}}" alt="imagen producto" class="w3->round">
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

</div>
@endsection

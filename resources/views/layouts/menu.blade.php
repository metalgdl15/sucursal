      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
          <li class="nav-item">
              <a class="nav-link" href="{{route('usuario.index')}}">

                  <span>Ver Perfil</span>
              </a>
          </li>

        <li class="nav-item active">
          <a class="nav-link" href="{{route ('producto.create')}}">    
            <i class="fas fa-fw fa-chart-area"></i>
            <span>crear producto</span>
          </a>
        </li>
        
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{route ('producto.index')}}" id="pagesDropdown">
            <i class="fas fa-fw fa-table"></i>
            <span>mostrar productos</span>
          </a>
          
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{route ('venta.index')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>compras</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="tables.html">    
            <span>Esto solo esta de adorno xD</span>
          </a>
        </li>

      </ul>

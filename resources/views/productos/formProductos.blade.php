@extends ('layouts.tema')
@section('contenido')

    <div class="row">
      <div clas="col-md-12">
          <div class="tile">
           @if(isset($producto))
       
         <form action="{{ route('producto.update', $producto->id) }}" method="POST"> 
         <input type="hidden" name="_method" value="PATCH"> 
      @else
       
          <form action="{{ route('producto.store') }}" method="POST" enctype="multipart/form-data">
      @endif
       
            {{ csrf_field() }}
         
            @if ($errors->any())
                <div class="alert alert-danger">
                   <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif

           
            
             <div class="form-group">
                <label for="nombre">Nombre Producto</label>
                <input name ="nombre" class="form-control" type="text">
             </div>
            
             <div class="form-group">
                <label for="descripcion">Descripcion</label>
                <textarea name="descripcion" class="form-control" type="text"> </textarea>
                
             </div>
            
             <div class="form-group">
                <label for="cantidad">Cantidad</label>
                <input name="cantidad" class="form-control" type="text">
         
             </div>

             <div class="form-group">
                <label for="existencia">Existencia</label>
                <input name="existencia" class="form-control" type="text">
             </div>

             <div class="form-group">
                <label for="precio">Precio</label>
                <input name="precio" class="form-control" type="text">
             </div>

             <div class ="form-goup">
                 <input name="photo" type="file" class="form-control" required>
             </div>
            
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Agregar</button>
            </div>
            
          </form>
        </div>
      </div>
    </div>

@endsection

@extends ('layouts.tema')
@section('contenido')

<div class="row">
  <div class="col-md-12">
    <table>
      <thead>
        <tr>
         

          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Precio</th>

            
        </tr>
      </thead>

      <tbody>
        @foreach($producto as $prod)
          <tr>
  
            <td width="100">{{ $prod->nombre }}</td>  
            <td width="100">{{ $prod->descripcion }} </td>
            <td width="100">{{ $prod->precio }} </td> 
            <td> 
              <img src="/img/productos/{{$prod->photo}}" alt="imagen producto" class="w3->round"> 
            </td>
            <td>
              <a class="btn btn-sm bt-info btn-primary" href ="{{route('producto.show', $prod->id)}}"> Ver Detalle </a>
            </td>
             
          </tr>
          <td>
        
                
        @endforeach
      </tbody>
    </table>

             {{$producto->links()}}

     

  </div>

</div>

@endsection
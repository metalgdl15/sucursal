@extends ('layouts.tema')
@section('contenido')
<div class="row">
  <div class="col-md-12">
    <table>
      <thead>
        <tr>
          <th>ID </th>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Cantidad</th>
          <th>Existencia</th>
          <th>Precio</th>
        </tr>
      </thead>

      <img src="/img/productos/{{$producto->photo}}" alt="imagen producto" class="w3->round">
      <tbody>
        
          <tr>

            <td>{{ $producto->id }}</td>
            <td>{{ $producto->nombre }}</td>
            <td>{{ $producto->descripcion }}</td>
            <td>{{ $producto->cantidad }}</td>
            <td>{{ $producto->existencia }}</td>
            <td>{{ $producto->precio }}</td>

          </tr>
        
      </tbody>
    </table>

    <a class="btn btn-warning" href="{{ route('producto.edit', $producto->id) }}">Editar</a>
                <hr>

     <form action="{{ route('producto.destroy', $producto->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>

    

    <form action="{{route ('venta.producto', $producto->id)}}", method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="precio" value="{{$producto->precio}}">
      <button type="submit" class="btn btn-danger">Comprar</button>
    </form>
  </div>

</div>

@endsection
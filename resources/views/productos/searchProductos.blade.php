@extends ('layouts.tema')
@section('contenido')

<div class="row">
  <div class="col-md-12">
    <table>
      <thead>
        <tr>
          <th>  </th>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Precio</th>
        </tr>
      </thead>

      <tbody>
        @foreach($producto as $prod)
          <tr>
            <td>
              <a class="btn btn-sm bt-info btn-primary" href ="{{route('producto.show', $prod->id)}}"> Ver Detalles </a>
            </td>

            <td>{{ $prod->nombre }}</td>  
            <td>{{ $prod->descripcion }} </td>
            <td>{{ $prod->precio }} </td> 
            <td> 
              <img src="/img/productos/{{$prod->photo}}" alt="imagen producto" class="w3->round"> 
            </td>
          </tr>
          
        @endforeach
      </tbody>
    </table>
    {{$producto->links()}}
  </div>

</div>

@endsection
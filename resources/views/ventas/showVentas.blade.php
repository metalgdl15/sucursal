@extends ('layouts.tema')
@section('contenido')
<div class="row">
  <div class="col-md-12">
    <table>
      <thead>
        <tr>
          <th>ID </th>
          <th>Usuario</th>
          <th>Fecha</th>
          <th>Total</th>
        </tr>
      </thead>

      <tbody>
          <tr>

            <td>{{ $venta->idVentas}}</td>
            <td>{{ $venta->idUsuario }}</td>
            <td>{{ $venta->fecha }}</td>
            <td>{{ $venta->total }}</td>
           
          </tr>
        
      </tbody>
    </table>

    <!-- tiene que mandarte a ventas con el formulario-->
      <input type="hidden" name="_method" value="Comprar">
      <button type="submit" class="btn btn-danger">Comprar</button>
  </div>

</div>

@endsection
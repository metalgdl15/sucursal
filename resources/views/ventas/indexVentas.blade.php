@extends ('layouts.tema')
@section('contenido')

<div class="row">
  <div class="col-md-12">
    <table>
      <thead>
        <tr>
          <th>  </th>
          <th>ID</th>
          <th>Usuario</th>
          <th>Fecha</th>
          <th>Total</th>

        </tr>
      </thead>

      <tbody>
        @foreach($ventas as $vent)
          <tr>
            <td>
              <a class="btn btn-sm bt-info btn-primary" href ="{{route('venta.detalle', $vent->productos->first()->id)}}"> Ver Detalles </a>
            </td>

            <td>{{ $vent->idVenta}}</td>  
            <td>{{ $vent->idUsuario }} </td>
            <td>{{ $vent->fecha }} </td> 
            <td>{{ $vent->total }} </td> 
            
          </tr>
          
        @endforeach
      </tbody>
    </table>
  </div>

</div>

@endsection